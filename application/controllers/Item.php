<?php

class Item extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('item_model');
		$this->load->model('category_model');
	}		

	// To display Item listing page
	public function index(){
		
		// To get records from items table
		$aItemCategories = $this->item_model->get_all_items();

		$aItems = array();
		foreach($aItemCategories as $key => $oItemCategory):
			if( array_key_exists( $oItemCategory->item_id , $aItems )){
				$aItems[$oItemCategory->item_id]['category_ids'][$key] = $oItemCategory->category_id;
			}else{
				$aItems[$oItemCategory->item_id]['item_id'] = $oItemCategory->item_id;
				$aItems[$oItemCategory->item_id]['item_name'] = $oItemCategory->item_name;
				$aItems[$oItemCategory->item_id]['description'] = $oItemCategory->description;
				$aItems[$oItemCategory->item_id]['category_ids'][$key] = $oItemCategory->category_id;
			}
		endforeach;
			//var_dump($aItems);exit;
		$aData['aItems'] = $aItems;
			
		// To get records from categories table
		$aData['aCategories'] = $this->category_model->get_all_categories();
			
		echo $this->load->template('item_list' , $aData , true );
	}

	// To add new record and update existing record
	public function add_edit_item(){
			
		if($this->input->post('item_name')){
			$aDataItem['item_name'] = $this->input->post('item_name');
			$aDataItem['description'] = $this->input->post('item_desc');
			$aDataItem['category_ids'] = $this->input->post('category_ids');		
			
			if( $this->input->post('item_id') == '' ){
				// save new item
				$aDataItem['created_at'] = date('Y-m-d H:i:s');
				$this->item_model->save_new_item( $aDataItem );
			}else{
				// edit existing item
				$this->item_model->update_item( $aDataItem , $this->input->post('item_id') );
			}
		}
		redirect('item');
	}

	// To remove category
	public function remove_item(){
		if( $this->input->post('item_id') ){
			// Remove from items table
			$this->item_model->remove_item( $this->input->post('item_id') );
		}
	}

	// To display item details
	public function item_detail(){
		// To get records from categories table
		$aData['aCategories'] = $this->category_model->get_all_categories();

		// Get the result from items table
		$aWhereItem['id'] = $this->uri->segment(3);
		$aData['oItem'] = $this->item_model->get_item( $aWhereItem );
			
		// Get item categories
		$aData['aSelectedCategories'] = $this->item_model->get_item_categories( $this->uri->segment(3) );		
		
		$strSelectedCategoryIds = '';
		foreach($aData['aSelectedCategories'] as $key => $oCategory):
			if( $key == 0)	
				$strSelectedCategoryIds = $oCategory->id;
			else
				$strSelectedCategoryIds = $strSelectedCategoryIds.','.$oCategory->id;
		endforeach;
		$aData['strSelectedCategories'] = $strSelectedCategoryIds;
		
		echo $this->load->template( 'item_detail' , $aData , true );
	}
}
