<?php

class Category extends MY_Controller{
	
	public function __construct(){
		
		parent::__construct();
		$this->load->model('category_model');
	}		

	// To display category listing page
	public function index(){
		$aData['aCategories'] = $this->category_model->get_all_categories();
		echo $this->load->template('categories_list' , $aData , true );
	}

	// To save new category , also update the category record in categories table 
	public function add_edit_category(){
		if($this->input->post('category_name')){
			
			$aDataCategory['category_name'] = $this->input->post('category_name');
			
			if( $this->input->post('category_id') == '' ){
				// save new category
				$aDataCategory['created_at'] = date('Y-m-d H:i:s');
				$this->category_model->save_new_category( $aDataCategory );
			}else{
				// edit existing category
				$aWhereCategory['id'] = $this->input->post('category_id');
				$this->category_model->update_category( $aDataCategory , $aWhereCategory );
			}
		}
		redirect('category');
	}

	// To remove category
	public function remove_category(){
		if($this->input->post('category_id')){
			// Remove from categories table
			$this->category_model->remove_category( $this->input->post('category_id') );
		}
	}

	// To display category details
	public function category_detail(){
		
		$aWhereCategory['id'] = $this->uri->segment(3);
		
		// Get the result from categories table
		$aData['oCategory'] = $this->category_model->get_category( $aWhereCategory );
		echo $this->load->template( 'category_detail' , $aData , true );
	}
}
