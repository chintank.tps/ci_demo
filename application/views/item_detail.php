
    <div class="row">
        <div class=" col-xs-12 col-sm-12">
            <!-- <div class="inner_page_wrap"> -->
                <div class="page_title view-page-title">
                    <h2 class="mt-10">Category Details</h2>
                </div>
            <!-- </div> -->
        </div>
    </div>
    <!-- page title row ends here-->

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row custom-form-row-full">
                            <button href="javascript:;" data-target="#item_modal" class="action-link edit_item_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-id="<?php echo $oItem->id; ?>" data-name="<?php echo $oItem->item_name; ?>" data-description="<?php echo $oItem->description; ?>" data-category-ids="<?php echo $strSelectedCategories; ?>" data-toggle="modal"  >Edit</button>
                            <button class="remove_item_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-item-id="<?php echo $oItem->id; ?>"  href="javascript:;">Delete</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <!-- table wrapper starts here -->
    <div class="row table-contents-wrapper table-records-wrapper">
        <div class="col-xs-12 revenue-table-wrapper">
            <div class="table-responsive custom-record-table">
                <table class="table table-hover revenue-table">
                    <thead class="bg-color"></thead>
                    <tbody>
                        <tr>
                            <th>Item Name</th>
                            <td><?php echo $oItem->item_name; ?></td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td><?php echo $oItem->description; ?></td>
                        </tr>
                        <tr>
                            <th>Categories</th>
                            <td>
                                <?php 
                                    foreach($aSelectedCategories as $key => $oCategory):
                                        echo $oCategory->category_name.'<br>';
                                    endforeach;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Created At</th>
                            <td><?php echo date('d M , Y',strtotime($oItem->created_at)); ?></td>
                        </tr>
                        <tr>
                            <th>Updated at</th>
                            <td><?php echo date('d M , Y',strtotime($oItem->updated_at)); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- table wrapper ends here -->

<?php require_once('modals/item_modal.php'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom-js/item.js"></script>