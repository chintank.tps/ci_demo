    <div class="row">
        <div class=" col-xs-12 col-sm-12">
            <!-- <div class="inner_page_wrap"> -->
                <div class="page_title view-page-title">
                    <h2 class="mt-10">Category</h2>
                </div>
            <!-- </div> -->
        </div>
    </div>
    <!-- page title row ends here-->

    <!-- form -->
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row custom-form-row-full">
                            <button data-target="#category_modal" class="add_category_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-toggle="modal" href="javascript:;"><span class="glyphicon glyphicon-plus"></span> Add New Category</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- table wrapper starts here -->
    <div class="row table-contents-wrapper table-records-wrapper">
        <div class="col-xs-12 revenue-table-wrapper">
            <div class="table-responsive custom-record-table">
                <table class="table table-hover revenue-table">
                    <thead class="bg-color">
                        <tr>
                            <th>Category name</th>
                            <th>No of Items</th>
                            <th class="width-10">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php if(!empty($aCategories)){ ?>
                            <?php foreach($aCategories as $key => $oCategory): ?>
                                <tr>
                                    <td><?php echo $oCategory->category_name; ?></td>
                                    <td><?php echo $oCategory->no_of_items; ?></td>
                                    <td>
                                        <a href="javascript:;" data-target="#category_modal" class="action-link edit_category_btn" data-id="<?php echo $oCategory->id; ?>" data-name="<?php echo $oCategory->category_name; ?>"  data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="javascript:;" class="action-link remove_category_btn" data-id="<?php echo $oCategory->id; ?>"><i class="glyphicon glyphicon-trash"></i> </a>
                                        <a href="<?php echo base_url(); ?>category/category_detail/<?php echo $oCategory->id; ?>" class="action-link"><i class="glyphicon glyphicon-eye-open"></i> </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr><td colspan="3"><h2 >No Record Found</h2></td></tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- table wrapper ends here -->

                          
<?php require_once('modals/category_modal.php'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom-js/category.js"></script>