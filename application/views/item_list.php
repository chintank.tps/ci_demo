
    <div class="row">
        <div class=" col-xs-12 col-sm-12">
            <!-- <div class="inner_page_wrap"> -->
                <div class="page_title view-page-title">
                    <h2 class="mt-10">Item</h2>
                </div>
            <!-- </div> -->
        </div>
    </div>
    <!-- page title row ends here-->

    <!-- form -->
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row custom-form-row-full">
                            <button data-target="#item_modal" class="add_item_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-toggle="modal" href="javascript:;"><span class="glyphicon glyphicon-plus"></span> Add New Item</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- table wrapper starts here -->
    <div class="row table-contents-wrapper table-records-wrapper">
        <div class="col-xs-12 revenue-table-wrapper">
            <div class="table-responsive custom-record-table">
                <table class="table table-hover revenue-table">
                    <thead class="bg-color">
                        <tr>
                            <th>Item name</th>
                            <th>Description</th>
                            <th class="width-10">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php if(!empty($aItems)){ ?>
                            <?php foreach($aItems as $key => $aItem):  $sCategoryIds = implode(',',$aItem['category_ids']); ?>
                                <tr>
                                    <td><?php echo $aItem['item_name']; ?></td>
                                    <td><?php echo $aItem['description'];?></td>
                                    <td>
                                        <a href="javascript:;" data-target="#item_modal" class="action-link edit_item_btn" data-id="<?php echo $aItem['item_id']; ?>" data-name="<?php echo $aItem['item_name']; ?>" data-description="<?php echo $aItem['description']; ?>" data-category-ids="<?php echo $sCategoryIds; ?>"  data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="javascript:;" class="action-link remove_item_btn"  data-item-id="<?php echo $aItem['item_id']; ?>"><i class="glyphicon glyphicon-trash"></i> </a>
                                        <a href="<?php echo base_url(); ?>item/item_detail/<?php echo $aItem['item_id']; ?>" class="action-link"><i class="glyphicon glyphicon-eye-open"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                                <tr><td colspan="3"><h2 >No Record Found</h2></td></tr>
                        <?php } ?>
                            
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- table wrapper ends here -->            

<?php require_once('modals/item_modal.php'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom-js/item.js"></script>



