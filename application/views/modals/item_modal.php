 <!--item modal starts here -->
<div class="modal fade" id="item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Item</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" name="add_item_frm" id="add_item_frm" action="<?php echo base_url(); ?>item/add_edit_item" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="camera_name" class="col-sm-4 control-label">Item Name</label>
                            <div class="col-sm-6 edit-time" id="class_fields">
                                <input type="hidden" name="item_id" id="item_id">
                                <input type="text" placeholder="Item Name" class="form-control custom-form-control" name="item_name" id="item_name" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="camera_name" class="col-sm-4 control-label">Description</label>
                            <div class="col-sm-6 edit-time" id="class_fields">
                                <textarea name="item_desc" id="item_desc" class="form-control custom-form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="camera_name" class="col-sm-4 control-label">Category</label>
                            <div class="col-sm-6 edit-time" id="class_fields">
                                <?php /*<select data-placeholder="Select Category" multiple class="chosen-select" style="width:350px;" tabindex="18" id="multiple-label-example">
                                    <?php foreach($aCategories as $key => $oCategory): ?>
                                        <option value="<?php echo $oCategory->id; ?>"><?php echo $oCategory->category_name; ?></option>    
                                    <?php endforeach; ?>
                                </select>*/ ?>
                                <select id="category_ids" name="category_ids[]" size="5" multiple class="form-control custom-form-control">
                                    <?php foreach ($aCategories as $key => $oCategory): ?>
                                        <option value="<?php echo $oCategory->id; ?>" ><?php echo $oCategory->category_name; ?></option>        
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>  
                    </div>           
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn custom-btn custom_btn btn-gray" data-dismiss="modal">Close</button>
                <button type="button" class="btn custom-btn custom_btn custom-save-btn" name="Save" id="save_item_btn" style="margin-bottom: 8px;" >Save</button>
            </div>
        </div>
    </div>
</div>
<!-- item modal ends here-->