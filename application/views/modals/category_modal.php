<!-- category modal starts here -->
<div class="modal fade" id="category_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Category</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" name="add_category_frm" id="add_category_frm" action="<?php echo base_url(); ?>category/add_edit_category" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="camera_name" class="col-sm-4 control-label">Category Name</label>
                            <div class="col-sm-6 edit-time" id="class_fields">
                                <input type="hidden" name="category_id" id="category_id">
                                <input type="text" placeholder="Category Name" class="form-control custom-form-control" name="category_name" id="category_name" value="">
                            </div>
                        </div>
                    </div>           
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn custom-btn custom_btn btn-gray" data-dismiss="modal">Close</button>
                <button type="button" class="btn custom-btn custom_btn custom-save-btn" name="Save"  id="save_category_btn" style="margin-bottom: 8px;">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- category modal ends here-->