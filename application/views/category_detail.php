
    <div class="row">
        <div class=" col-xs-12 col-sm-12">
            <!-- <div class="inner_page_wrap"> -->
                <div class="page_title view-page-title">
                    <h2 class="mt-10">Category Details</h2>
                </div>
            <!-- </div> -->
        </div>
    </div>
    <!-- page title row ends here-->

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row custom-form-row-full">
                            <button href="javascript:;" data-target="#category_modal" class="action-link edit_category_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-id="<?php echo $oCategory->id; ?>" data-name="<?php echo $oCategory->category_name; ?>"  data-toggle="modal"  >Edit</button>
                            <button class="remove_category_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-id="<?php echo $oCategory->id; ?>"  href="javascript:;">Delete</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <!-- table wrapper starts here -->
    <div class="row table-contents-wrapper table-records-wrapper">
        <div class="col-xs-12 revenue-table-wrapper">
            <div class="table-responsive custom-record-table">
                <table class="table table-hover revenue-table">
                    <thead class="bg-color"></thead>
                    <tbody>
                        <tr>
                            <th>Category Name</th>
                            <td><?php echo $oCategory->category_name; ?></td>
                        </tr>
                        <tr>
                            <th>Created At</th>
                            <td><?php echo date('d M , Y',strtotime($oCategory->created_at)); ?></td>
                        </tr>
                        <tr>
                            <th>Updated at</th>
                            <td><?php echo date('d M , Y',strtotime($oCategory->updated_at)); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- table wrapper ends here -->

<?php require_once('modals/category_modal.php'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom-js/category.js"></script>