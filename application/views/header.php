<script type="text/javascript"> var base_url = '<?php echo base_url(); ?>';</script>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="minimal-ui, width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>CI Demo</title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.css">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/default.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/demo-form.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/add_new_school.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/view_reports.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/color_theme.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/chosen/chosen.css">

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/script.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/retina.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/sweetalert2.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/chosen/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {
                allow_single_deselect: true
            },
            '.chosen-select-no-single': {
                disable_search_threshold: 10
            },
            '.chosen-select-no-results': {
                no_results_text: 'Oops, nothing found!'
            },
            '.chosen-select-width': {
                width: "95%"
            }
        }
        for(var selector in config) {
            $(selector).chosen(config[selector]);
        }
    </script>
</head>

<body>
    <div class="page-wrapper">
        <div id="wrapper" class="toggled">
            <!-- Sidebar Start-->
            <ul class="sidebar_wrap navbar navbar-inverse navbar-fixed-top col-xs-12" id="sidebar-wrapper" role="navigation">
                <li class="col-xs-12">
                    <a href="<?php echo base_url(); ?>category/index" class="main-link col-xs-12" title="Users">                        
                        <span><i class="fa fa-users"></i><span>Category</span></span>
                    </a>
                </li>

                <li class="col-xs-12">
                    <a href="<?php echo base_url(); ?>item/index" class="main-link col-xs-12" title="Students">                        
                        <span><i class="fa fa-graduation-cap"></i><span>Items</span></span>
                    </a>
                </li>
            </ul>
            <!-- Sidebar End-->
            <!-- Main page content including header-->
            <div id="page-content-wrapper">
                <!-- Header start -->
                <div class="header_wrap navbar-fixed-top">
                    <div class="menu_icon" class="hamburger is-closed" id="leftbtn">
                        <i class="fa fa-bars"></i>
                    </div>
                    <div class="logo">
                        <a href="#"></a>
                    </div>
                </div>
                <!-- Header End-->
            