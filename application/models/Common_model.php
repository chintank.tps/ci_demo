<?php

class Common_model extends CI_Model
{
	/**
	 * @param null $sTableName
	 * @param array $aData
	 * @return bool
	 */
	public function  insert( $sTableName = NULL , $aData = array() )
	{
		if ( !empty( $sTableName ) AND !empty( $aData ) ) {
			return $this->db->insert( $sTableName , $aData );
		} else {
			return false;
		}
	}


	/**
	 * @param null $sTableName
	 * @param array $aData
	 * @param array $aWhere
	 * @return bool
	 */
	public function  update( $sTableName = NULL , $aData = array() , $aWhere = array() )
	{
		if ( !empty( $sTableName ) AND !empty( $aData ) AND !empty( $aWhere ) ) {
			$this->db->where( $aWhere );
			return $this->db->update( $sTableName , $aData );
		} else {
			return false;
		}
	}

	/**
	 * @param null $sTableName
	 * @param array $aWhere
	 * @return bool
	 */
	public function  delete( $sTableName = NULL , $aWhere = array() )
	{
		if ( !empty( $sTableName ) AND !empty( $aWhere ) ) {
			$this->db->where( $aWhere );
			return $this->db->delete( $sTableName );
		} else {
			return false;
		}
	}

	/**
	 * @param null $sTableName
	 * @param array $aWhere
	 * @return array
	 */
	public function  get_result( $sTableName = NULL , $aWhere = array() )
	{
		if ( !empty( $sTableName ) ) {
			$this->db->select( '*' );
			$this->db->from( $sTableName );
			if ( !empty( $aWhere ) )
				$this->db->where( $aWhere );
			return $this->db->get()->result();
		} else
			return array();
	}

	/**
	 * @param null $sTableName
	 * @param array $aWhere
	 * @return array
	 */
	public function  get_row( $sTableName = NULL , $aWhere = array() )
	{
		if ( !empty( $sTableName ) ) {
			$this->db->select( '*' );
			$this->db->from( $sTableName );
			if ( !empty( $aWhere ) )
				$this->db->where( $aWhere );
			return $this->db->get()->row();
		} else
			return array();
	}

	/**
	 * @param null $sQuery
	 * @return array
	 */
	public function  custom_query_result( $sQuery = NULL )
	{
		if ( !empty( $sQuery ) ) {
			return $this->db->query( $sQuery )->result();
		} else {
			return array();
		}
	}

	/**
	 * @param null $sQuery
	 * @return array
	 */
	public function  custom_query_row( $sQuery = NULL )
	{
		if ( !empty( $sQuery ) ) {
			return $this->db->query( $sQuery )->row();
		} else {
			return array();
		}
	}

	/**
	 * @param array $aData
	 * @return mixed
	 */
	public function  custom_query( $aData = array() )
	{
		//select fields array
		if ( isset( $aData[ 'aSelect' ] ) AND !empty( $aData[ 'aSelect' ] ) ) {
			$this->db->select( $aData[ 'aSelect' ] );
		} else {
			// all fields
			$this->db->select( '*' );
		}


		if ( isset( $aData[ 'aJoinTbl' ] ) AND !empty( $aData[ 'aJoinTbl' ] ) ) {
			// join table
			$loop = 1;
			foreach ( $aData[ 'aJoinTbl' ] as $row ) {
				if ( $loop == 1 ) {
					$this->db->from( $row[ 0 ] );
				} else {
					$this->db->join( $row[ 0 ] , $row[ 1 ] , $row[ 2 ] );
				}
				$loop ++;
			}
		} else {
			// without join
			$this->db->from( $aData[ 'sTableName' ] );
		}


		if ( isset( $aData[ 'aWhere' ] ) AND !empty( $aData[ 'aWhere' ] ) ) {
			// set where condition with array
			$this->db->where( $aData[ 'aWhere' ] );
		}

		if ( isset( $aData[ 'sWhere' ] ) AND $aData[ 'sWhere' ] != "" ) {
			// custom where with string
			$this->db->where( $aData[ 'sWhere' ] );
		}

		if ( isset( $aData[ 'iOffset' ] ) AND $aData[ 'iOffset' ] != - 1 ) {
			// set limit
			$this->db->limit( $aData[ 'iPerPage' ] );
			$this->db->offset( $aData[ 'iOffset' ] );
		}

		if ( isset( $aData[ 'aGroupBy' ] ) AND !empty( $aData[ 'aGroupBy' ] ) ) {
			// group by fields
			$this->db->group_by( $aData[ 'aGroupBy' ] );
		}

		if ( isset( $aData[ 'aSortBy' ] ) AND !empty( $aData[ 'aSortBy' ] ) ) {
			// sorting
			foreach ( $aData[ 'aSortBy' ] as $row ) {
				$this->db->order_by( $row[ 0 ] , $row[ 1 ] );
			}
		}

		return $this->db->get()->result();
	}

	/**
	 * @param null $sQuery
	 */
	public function  exec_query( $sQuery = NULL )
	{
		if( $sQuery != NULL )
		{
			$this->db->query($sQuery);
		}
	}

	/**
	* @param null $sTableName
	* @return array
	*/
	public function get_random_select()
	{
		$sQuery = "select `file_path`,`description` from kv_banner order by rand() limit 1";
		if( $sQuery != NULL )
		{
			return $this->db->query( $sQuery )->row();
		}	
	}
}