<?php 

class Category_model extends CI_Model{
	
	protected $sCategoriesTable = "categories";
	protected $sItemCategoriesTable = "item_categories";
	protected $sItemsTable = "items";

	// To get all categories with no of items
	public function get_all_categories(){
		$sQuery = "SELECT categories.* , COUNT(item_categories.id) as no_of_items FROM categories LEFT JOIN item_categories on categories.id = item_categories.category_id GROUP BY categories.id";
		return $this->common_model->custom_query_result($sQuery);
	}

	// To get any specifix category
	public function get_category( $aWhere ){
		return $this->common_model->get_row( $this->sCategoriesTable , $aWhere );
	}

	// To insert new record in categories table
	public function save_new_category( $aInputData ){
		$this->common_model->insert( $this->sCategoriesTable , $aInputData );
	}

	// To update existing record in categories table
	public function update_category( $aInputData , $aWhere ){
		$this->common_model->update( $this->sCategoriesTable , $aInputData , $aWhere );
	}

	// To remove category related records
	public function remove_category( $sCategoryId ){
		
		// Remove from categories table
		$aWhereCategory['id'] = $sCategoryId;
		$this->common_model->delete( $this->sCategoriesTable , $aWhereCategory );	

		// Remove from item_categories table
		$aWhereItemCategory['category_id'] = $sCategoryId;
		$aCategoriesItems = $this->common_model->get_result( $this->sItemCategoriesTable , $aWhereItemCategory );

		if( !empty($aCategoriesItems) ){
			$this->common_model->delete( $this->sItemCategoriesTable , $aWhereItemCategory );

			// Check item_id exist in item_categories table
			unset($aWhereItemCategory);
			$removeable_items_id = array();
			foreach($aCategoriesItems as $key => $oCatItem):
				$aWhereItemCategory['item_id'] = $oCatItem->item_id;
				$aCategories = $this->common_model->get_result( $this->sItemCategoriesTable , $aWhereItemCategory );
				if(empty( $aCategories )){
					$removeable_items_id[$key] = $oCatItem->item_id;
				}
			endforeach;
				
			// Remove from items table
			if(!empty($removeable_items_id))
				$this->db->where_in('id', $removeable_items_id)->delete( $this->sItemsTable );
		}
	}
}