<?php 

class Item_model extends CI_Model{
	
	protected $sCategoriesTable = "categories";
	protected $sItemCategoriesTable = "item_categories";
	protected $sItemsTable = "items";

	// To get all items
	public function get_all_items(){
		/*return $this->common_model->get_result( $this->sItemsTable );*/
		$sQuery = "select item_categories.*,items.item_name,items.description from item_categories RIGHT JOIN items on item_categories.item_id = items.id";
		return $this->common_model->custom_query_result( $sQuery );			
	}

	// To get any specific item
	public function get_item( $aWhere ){
		return $this->common_model->get_row( $this->sItemsTable , $aWhere );
	}

	public function get_item_categories( $item_id ){
		$sQuery = "select categories.* from item_categories INNER JOIN categories on item_categories.category_id = categories.id where item_categories.item_id = ".$item_id;
		return $this->common_model->custom_query_result( $sQuery );		
	}

	// To insert new record in item
	public function save_new_item( $aItemDetail ){
		
		$aInputItem['item_name'] = $aItemDetail['item_name']; 
		$aInputItem['description'] = $aItemDetail['description']; 
		$aInputItem['created_at'] = $aItemDetail['created_at'];

		// Save record in items table
		$this->common_model->insert( $this->sItemsTable , $aInputItem );
		$item_id = $this->db->insert_id();

		// save records in item_categories table
		$aInputItemCategories =  array();
		
		foreach($aItemDetail['category_ids'] as $key => $sCategoryId):
			$aInputItemCategories[$key]['item_id'] = $item_id;
			$aInputItemCategories[$key]['category_id'] = $sCategoryId;
		endforeach;

		$this->db->insert_batch( $this->sItemCategoriesTable , $aInputItemCategories );
	}

	public function update_item( $aItemDetail , $item_id ){
		$aInputItem['item_name'] = $aItemDetail['item_name']; 
		$aInputItem['description'] = $aItemDetail['description']; 
		$aWhere['id'] = $item_id;
		
		// To update record in items table
		$this->common_model->update( $this->sItemsTable , $aInputItem , $aWhere );

		// remove records from item_categories table
		$aWhereItemCategory['item_id'] = $item_id;
		$this->common_model->delete( $this->sItemCategoriesTable , $aWhereItemCategory );

		// save records in item_categories table
		$aInputItemCategories =  array();
		
		foreach($aItemDetail['category_ids'] as $key => $sCategoryId):
			$aInputItemCategories[$key]['item_id'] = $item_id;
			$aInputItemCategories[$key]['category_id'] = $sCategoryId;
		endforeach;
		
		$this->db->insert_batch( $this->sItemCategoriesTable , $aInputItemCategories );
	}

	public function remove_item( $item_id ){
		
		// Remove from items table
		$aWhereItem['id'] = $item_id;
		$this->common_model->delete( $this->sItemsTable , $aWhereItem );

		// Remove from item_categories table
		$aWhereItemCategory['item_id'] = $item_id;
		$this->common_model->delete( $this->sItemCategoriesTable , $aWhereItemCategory );
	}
}
