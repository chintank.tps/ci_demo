<?php


class MY_Loader extends CI_Loader
{
	public function  __construct(){
		parent::__construct();
	}

	/**
	 * @param string $sTemplateName
	 * @param array $aVar
	 * @param bool $bReturn
	 * @return string
	 */
	public function  template( $sTemplateName = "" , $aVar = array() , $bReturn = false ){
		
		$sContent = $this->view( 'header' , $aVar , $bReturn );
		$sContent .= $this->view( $sTemplateName , $aVar , $bReturn );
		$sContent .= $this->view( 'footer' , $aVar , $bReturn );
			
		return $sContent;
	}
}