
$( document ).ready(function(){
    $('#save_category_btn').on('click',function(){
        
        var category_id = $("#category_id").val();
        var category_name = $("#category_name").val();

        if( category_name == '' ){
            $("#category_name").css('border','1px solid red');
        }else{
            $("#add_category_frm").submit();
        }
    });

    // click of Add New Category button
    $('.add_category_btn').on('click',function(){
        $("#category_name").val('').css('border','');
    });

    // click of edit edit category icon
    $('.edit_category_btn').on('click',function(){
        var category_id = $(this).data('id');
        var category_name = $(this).data('name');

        $("#add_category_frm #category_id").val(category_id);
        $("#add_category_frm #category_name").val(category_name);
    });

    // click of remove category icon
    $('.remove_category_btn').on('click',function(){
        var category_id = $(this).data('id');
            //console.log( category_id );return false;
        swal({
            title: 'Are you sure?',
            text: "Deleting a Category will also remove related items. You won't be able to revert this!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
        }).then(function( isConfirm) {
            if(isConfirm){
                $.ajax({
                    url: base_url + "category/remove_category",
                    type: "POST",
                    data: {"category_id":category_id },
                    success: function (data) {
                        swal("Done!", "It was succesfully deleted!", "success");
                        $('.swal2-confirm').on('click',function(){
                            window.location.href = base_url + 'category';
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
            }
        });
    });      
});    