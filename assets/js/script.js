var page_name = '';
var iconMode = false;
var isMenuOpen = false;
var createdSubmenu = false;

/*Fetch all the settings required for the page */
function getSettings() {
    $.ajax({
        url: 'https://dateapi.herokuapp.com/settings',
        type: 'GET',
        dataType: 'json',
    }).done(function(data) {
        var sidemenu = data['response'].sidemenu
        var topmenu = data['response'].topmenu
        if (page_name == 'setting') {
            $('#myonoffswitch1').val(sidemenu).prop('checked', sidemenu);
            $('#myonoffswitch2').val(topmenu).prop('checked', topmenu);
        }
        if (sidemenu == 'true' || sidemenu == true) {
            iconMode = true;
            //$('#leftbtn').toggleClass('only_icon');
            // $("ul.sidebar_wrap").toggleClass('sidebar-wrapper-width');
            // $('#wrapper').toggleClass('small_icons');
            // if ($('#leftbtn').hasClass('only_icon')) {
            //     first = 0;
            // }
        }
        if (topmenu == 'false' || topmenu == false) {

            $('.header_wrap').toggleClass('navbar-fixed-top');
            $('.circle-chart-wrap').toggleClass('cutom_mrgn');

            if ($('.header_wrap').hasClass('navbar-fixed-top')) {
                $(this).attr('data-original-title', 'Unlock header to top');
                $(this).find('img').attr('src', 'images/icons/pin_icon.png');
            } else {
                $(this).attr('data-original-title', 'Lock header to top');
                $(this).find('img').attr('src', 'images/icons/unpin_icon.png');
            }

        }
        $('#myonoffswitch1').val(sidemenu).prop('checked', sidemenu);
        $('#myonoffswitch2').val(topmenu).prop('checked', topmenu);

    });
}


/*Functions for the homepage - Dashboard */

function fetchHomepageData() {
    $.ajax({
        url: 'https://dateapi.herokuapp.com/chart_contry_data',
        type: 'GET',
        dataType: 'json',
    }).done(function(data) {
        showPieChart(data.response);
    });

    $.ajax({
        url: 'http://dateapi.herokuapp.com/chart_date_data',
        type: 'GET',
        dataType: 'json',
    }).done(function(data) {
        showLinechart(data.response);
    });
    var i = 0,
        j = 0,
        v = 0,
        w = 0;
    var cls_name = document.getElementById('example');
    var cls_name1 = document.getElementById('example1');
    var cls_name2 = document.getElementById('example2');
    var cls_name3 = document.getElementById('example3');
    var r1 = new radialProgress(cls_name, '#303C45', 80);
    var r2 = new radialProgress(cls_name1, '#80BF4C', 80);
    var r3 = new radialProgress(cls_name2, '#0099D4', 80);
    var r4 = new radialProgress(cls_name3, '#ED4122', 80);
    setInterval(function() {
        r1.setValue(i);
        r2.setValue(j);
        r3.setValue(v);
        r4.setValue(w);
        i++;
        j++;
        v++;
        w++;
        if (i > 50) i = 50;
        if (j > 62) j = 62;
        if (v > 80) v = 80;
        if (w > 75) w = 75;
    }, 30);
}


/* checkbox event for revenue rule set page */
var revenueCheckboxCount = 0;
$('.revenuetbl-checkbox :checkbox').each(function() {
    $(this).click(function(){
      if(this.checked){
        revenueCheckboxCount++;
        if(revenueCheckboxCount > 10){
          revenueCheckboxCount = 1;
        }
      }
      else{
        revenueCheckboxCount--;
        if(revenueCheckboxCount < 1){
          revenueCheckboxCount = 10;
        }
      }
      if(revenueCheckboxCount >= 1){
      $(".edit-row").show();
        if(revenueCheckboxCount >= 10){
          $(".edit-row").hide();
        }
     }
     else{
      $(".edit-row").hide();
     }
    });
});
//$(".revenuetbl-checkbox").change(function() {
  // if(revenueCheckboxCount > 0){
  //     $(".edit-row").show();
  // }
  // else{
  //    $(".edit-row").hide();
  // }
    // if (this.checked) {
    //     $(".edit-row").show();
    // } else {
    //     $(".edit-row").hide();
    // }
//});


/*Common function */
function initCommonFunctions() {
    


    // To adjust the width of page wrap on mobile
    if ($(window).width() < 520) {
        $('#page-content-wrapper').width($(window).width()).css('overflow-x', 'hidden');
    } else {
        $('#page-content-wrapper').css({
            width: 'auto',
            'overflow-x': 'auto',
        });
    }

    $('#leftbtn').click(function() {

        $('#wrapper').toggleClass('toggled');

        isMenuOpen = $('#wrapper').hasClass('toggled')

        //When micro menu mode
        if (iconMode == true && isMenuOpen == true) {

            //Small_icons = margin-left:70px
            $('#wrapper').addClass('small_icons');

            //Add class to side bar
            $('ul.sidebar_wrap').addClass('sidebar-wrapper-width');
            $('.sidebar-wrapper-width a span span').addClass('hideMenu');
            $('.sidebar-wrapper-width a .menu-arrow').addClass('hideMenu');


            //Create floating menu dynamically
            $('.submenu').each(function(i) {
                if (createdSubmenu == false) {
                    var tmp_html = $(this).html();
                    var custom_html = '<div class=\'custom_tooltip\' id=\'customtools' + i + '\'\'>' + tmp_html + '</div>';
                    $(this).parent().append(custom_html);
                    $('div.custom_tooltip').hide();
                }
            });



            createdSubmenu = true;


            //Hide normal submenu
            $('#sidebar-wrapper li ul').addClass('hideMenu');
        } else {
            $('ul.sidebar_wrap').removeClass('sidebar-wrapper-width');
            $('#wrapper').removeClass('small_icons');
        }

        if (iconMode == true) {
            //Hide visible floating menu
            $('.sidebar_wrap li').on('click', function() {
                if ($(this).find('div.custom_tooltip').is(':visible')) {
                    $(this).find('div.custom_tooltip').hide();
                } else {
                    $('#sidebar-wrapper').addClass('sidebar-wrapper-sh');
                    $('div.custom_tooltip').hide();
                    $(this).find('div.custom_tooltip').show();

                }
            });
        }
    });

    $(document).click(function(event) {
        if (!$(event.target).closest('#sidebar-wrapper li').length) {
            if ($('#sidebar-wrapper li div.custom_tooltip').is(':visible')) {
                $(this).find('div.custom_tooltip').hide();
            }
        }
    })


    if ($(window).width() <= 768) {

        $('.header_wrap , #wrapper').css('width', '100%');
    }

    $('#sidebar-wrapper li').on('click', function() {


        if ($(this).find('ul').is(':visible')) {
            $('#sidebar-wrapper li span.glyphicon').removeClass('glyphicon-chevron-down');
            $('#sidebar-wrapper li span.glyphicon').addClass('glyphicon-chevron-right');
            $(this).find('ul').slideUp();
            $(this).find('span.glyphicon').removeClass('glyphicon-chevron-down');
            $(this).find('span.glyphicon').addClass('glyphicon-chevron-right');
        } else {
            $('#sidebar-wrapper li span.glyphicon').removeClass('glyphicon-chevron-down');
            $('#sidebar-wrapper li span.glyphicon').addClass('glyphicon-chevron-right');
            $('#sidebar-wrapper li ul').slideUp();
            $(this).find('span.glyphicon').removeClass('glyphicon-chevron-right');
            $(this).find('span.glyphicon').addClass('glyphicon-chevron-down');
            $(this).find('ul').slideDown();
        }
    });


    // $('.icons-toggle-btn').click(function(event) {
    //   if ($(window).width() < 768) {
    //     $('.icons-wrap').toggle();
    //     if ($('.icons-wrap').is(':visible')) {
    //       $('.icons-toggle-btn i').removeClass('glyphicon glyphicon-chevron-down');
    //       $('.icons-toggle-btn i').addClass('glyphicon glyphicon-chevron-up');
    //     } else {
    //       $('.icons-toggle-btn i').removeClass('glyphicon glyphicon-chevron-up');
    //       $('.icons-toggle-btn i').addClass('glyphicon glyphicon-chevron-down');
    //     }
    //   } else {
    //     $('.icons-wrap').show();
    //   }
    // });

    $(window).resize(function(event) {
        // To adjust the width of page wrap on mobile
        if ($(window).width() < 520) {
            $('#page-content-wrapper').width($(window).width()).css('overflow-x', 'hidden');
        } else {
            $('#page-content-wrapper').css({
                width: 'auto',
                'overflow-x': 'auto',
            });
        }
        // To adjust the width of page wrap on mobile end

        // To set icons on revenue page
        setIcons();

        // To hide full screen functionality in small devices
        if ($(window).width() < 1024) {
            $(".fullscreen").hide();
        }
        else{
            $(".fullscreen").show();
        }
    });
}



// CUSTOM FUNCTION

function showPieChart(p_data) {
    var chart;
    // PIE CHART
    chart = new AmCharts.AmPieChart();
    console.log('AMCHARTS ready called');
    // Title of the chart
    // chart.addTitle("Visitors countries", 16);

    chart.dataProvider = p_data;
    chart.titleField = 'country';
    chart.valueField = 'visits';
    chart.sequencedAnimation = true;
    chart.startEffect = 'elastic';
    chart.innerRadius = '30%';
    chart.startDuration = 2;
    chart.labelRadius = 15;
    chart.balloonText = '[[title]]<br><span style=\'font-size:14px\'><b>[[value]]</b> ([[percents]]%)</span>';
    // The following two lines makes the chart 3D
    chart.depth3D = 10;
    chart.angle = 15;

    // WRITE
    chart.write('chartdiv');
}



function radialProgress(parent, color, radius, className) {

    this.parent = typeof parent !== 'undefined' ? parent : document.body;
    this.color = typeof color !== 'undefined' ? color : '#000000';
    this.radius = typeof radius !== 'undefined' ? radius : '100';
    this.className = typeof className !== 'undefined' ? className : '';

    this.canvasElement = document.createElement('canvas');

    if (this.className != '') {
        this.canvasElement.className = this.className;
    }

    this.canvasElement.width = 2 * this.radius;
    this.canvasElement.height = 2 * this.radius;
    this.parent.appendChild(this.canvasElement);

    this.ctx = this.canvasElement.getContext('2d');

    this.setValue = function(val) {

        this.value = typeof val !== 'undefined' ? val : 0;

        this.ctx.save();

        this.ctx.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);

        this.ctx.beginPath();
        this.ctx.fillStyle = this.color;
        this.ctx.arc(this.radius, this.radius, this.radius * 80 / 100, 0, Math.PI * 2, false);
        this.ctx.fill();
        this.ctx.closePath();

        this.ctx.beginPath();
        this.ctx.moveTo(this.radius, this.radius);
        this.ctx.fillStyle = this.color;
        this.ctx.arc(this.radius, this.radius, this.radius, -Math.PI / 2, this.value * Math.PI * 2 / 100 - Math.PI / 2, false);

        this.ctx.globalCompositeOperation = 'source-out';
        this.ctx.fill();
        this.ctx.closePath();

        this.ctx.restore();

        this.ctx.fillStyle = this.color;
        this.ctx.font = Math.round(this.radius / 2) + 'px Arial';
        this.ctx.textAlign = 'center';
        this.ctx.textBaseline = 'middle'
        this.ctx.fillText(this.value + '%', this.radius, this.radius);
    }
    this.setValue();
    return this;
}

function showLinechart(p_data) {
    var average = 90.4;
    // SERIAL CHART
    var lineChart = new AmCharts.AmSerialChart();

    lineChart.dataProvider = p_data;
    lineChart.categoryField = 'date';
    lineChart.dataDateFormat = 'YYYY-MM-DD';

    // AXES
    // category
    var categoryAxis = lineChart.categoryAxis;
    categoryAxis.parseDates = true; // As our data is date-based, we set parseDates to true
    categoryAxis.minPeriod = 'DD'; // Our data is daily, so we set minPeriod to DD
    categoryAxis.dashLength = 1;
    categoryAxis.gridAlpha = 0.15;
    categoryAxis.axisColor = '#DADADA';

    // Value
    var valueAxis = new AmCharts.ValueAxis();
    valueAxis.axisColor = '#DADADA';
    valueAxis.dashLength = 1;
    valueAxis.logarithmic = true; // This line makes axis logarithmic
    lineChart.addValueAxis(valueAxis);

    // GUIDE for average
    var guide = new AmCharts.Guide();
    guide.value = average;
    guide.lineColor = '#CC0000';
    guide.dashLength = 4;
    guide.label = 'average';
    guide.inside = true;
    guide.lineAlpha = 1;
    valueAxis.addGuide(guide);


    // GRAPH
    var graph = new AmCharts.AmGraph();
    graph.type = 'smoothedLine';
    graph.bullet = 'round';
    graph.bulletColor = '#FFFFFF';
    graph.useLineColorForBulletBorder = true;
    graph.bulletBorderAlpha = 1;
    graph.bulletBorderThickness = 2;
    graph.bulletSize = 7;
    graph.title = 'Price';
    graph.valueField = 'price';
    graph.lineThickness = 2;
    graph.lineColor = '#00BBCC';
    lineChart.addGraph(graph);

    // CURSOR
    var chartCursor = new AmCharts.ChartCursor();
    chartCursor.cursorPosition = 'mouse';
    lineChart.addChartCursor(chartCursor);

    // SCROLLBAR
    var chartScrollbar = new AmCharts.ChartScrollbar();
    chartScrollbar.graph = graph;
    chartScrollbar.scrollbarHeight = 30;
    lineChart.addChartScrollbar(chartScrollbar);

    lineChart.creditsPosition = 'bottom-right';

    // WRITE
    lineChart.write('chartdiv1');
}


function initSettingPageData() {

    $(document).on('click', ':checkbox', function() {
        var value = $(this).is(':checked')
        $(this).val(value);
    });
    $(document).on('click', '#save_settings', function(e) {
        var sideMenu = $('#myonoffswitch1').val();
        var topMenu = $('#myonoffswitch2').val();
        $.ajax({
                url: 'https://dateapi.herokuapp.com/settings/1',
                type: 'PUT',
                dataType: 'json',
                data: {
                    setting: {
                        topmenu: topMenu,
                        sidemenu: sideMenu,
                    },
                },
            })
            .done(function(data) {
                console.log(data['response'])
                window.location = 'dashboard.html';
            });

    });
}



/*Page load events */
$(document).ready(function() {
    $(".radio_btn.active_radio").find('.ovelay').css('display','block');
     $(".radio_btn").on('click',function(){
            $('.ovelay').css('display','none');
            $(".radio_btn").removeClass('active_radio');
            $(this).addClass('active_radio');
            $(this).find('.ovelay').css('display','block');
        });

    page_name = $('#pagename').attr('value');
    //Get settings of all pages
    
    getSettings();

    initCommonFunctions();

    if (page_name == 'home_page') {
        fetchHomepageData();
    } else if (page_name == 'setting') {
        initSettingPageData();
    }

    // Fullscreen Functionality
    if ($(window).width() < 1024) {
        $(".fullscreen").hide();
    }
    fullscreen();
    if (window.innerHeight == screen.height) {
        var element = $('.fullscreen');
        setTimeout(function() {
            console.log('inside timeout');
            $('.fullscreen').click();
            element.addClass('fullscreenExit').removeClass('fullscreen');
            element.find('i').attr('class', 'fa fa-compress');
        }, 1000);
    }


    // To set icons on revenue page
    $('#settingsIcon').click(function() {
        if ($('#moreIcons').is(':visible')) {
            $('#moreIcons').hide();
            if ($(window).width() < 767) {
                $('.table-icon-wrapper').removeClass('table-icon-wrapper-width-open');
            }


        } else {
            $('#moreIcons').show();
            if ($(window).width() < 767) {
                $('.table-icon-wrapper').addClass('table-icon-wrapper-width-open');
            }
        }
    });
    setIcons();
    
});


// To set icons on revenue page
function setIcons() {


    if ($(window).width() > 767) {
        $('.table-icon-wrapper').removeClass('table-icon-wrapper-width-open');
    } else {
        if ($('#moreIcons').is(':visible')) {
            $('.table-icon-wrapper').addClass('table-icon-wrapper-width-open');
        } else {
            $('.table-icon-wrapper').removeClass('table-icon-wrapper-width-open');
        }
    }
}

function fullscreen() {
    $(document).on('click', '.fullscreen', function(e) {
        console.log('fullscreen');
        $(this).addClass('fullscreenExit');
        $(this).removeClass('fullscreen');
        $(this).find('i').attr('class', 'fa fa-compress');
        e.preventDefault();
        var docElement, request;

        docElement = document.documentElement;
        request = docElement.requestFullScreen || docElement.webkitRequestFullScreen || docElement.mozRequestFullScreen || docElement.msRequestFullScreen;


        if (typeof request != 'undefined' && request) {
            request.call(docElement);
        }
        // Var iOS = /iPad|iPhone|iPod/.test(navigator.platform);
        // if (iOS) {
        //     window.scrollTo(0, 1);
        // }

    });

    $(document).on('click', '.fullscreenExit', function(e) {
        console.log('fullscreenexit');
        $(this).removeClass('fullscreenExit');
        $(this).addClass('fullscreen');
        $(this).find('i').attr('class', 'fa fa-expand');
        e.preventDefault();
        var docElement, request;

        docElement = document;
        request = docElement.cancelFullScreen || docElement.webkitCancelFullScreen || docElement.mozCancelFullScreen || docElement.msCancelFullScreen || docElement.exitFullscreen;
        if (typeof request != 'undefined' && request) {
            request.call(docElement);
        }
    });



}
